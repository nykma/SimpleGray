<hr/>
<p class="meta">
	Category: <a href="/categories/{{ post.category }}" class="category">{{ site.custom.category[post.category] }}</a>{% if post.date %} | 最初写作于 <span class="datetime">{{ post.date | date: "%Y-%m-%d" }}</span>{%endif%}
</p>
