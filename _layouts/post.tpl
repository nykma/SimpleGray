---
layout: page

pageClass: page-type-post

---
<!--
<div class="trace">/<a href="/">{{ site.name }}</a>/{{ page.title }}</div>
-->
{% include header.tpl %}
<article>
<!--	<h1><a href="{{ page.url }}">{{ page.title }}</a></h1>-->
	{% assign post = page %}
	{{ content }}
	
	{% include meta.tpl %}
	<!--<p class="permalink">永久链接：<a href="{{ permaurl }}">{{ permaurl }}</a></p>-->
</article>
<!--<div id="disqus_thread" class="comments"></div>-->
